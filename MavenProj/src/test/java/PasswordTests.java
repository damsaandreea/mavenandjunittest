import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;


import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;


public class PasswordTests {

    @Test
    void passwordValidator(){
        String password = "hhAg1oih%";
        assertEquals(true, PasswordValidator.validate(password));
    }

    @ParameterizedTest
    @ValueSource(strings = {"hhAg1oih%", "hhAg1oih%"}) // six numbers
    void isOdd_ShouldReturnTrueForOddNumbers(String word) {
        assertTrue(PasswordValidator.validate(word));
    }
    @ParameterizedTest
    @NullAndEmptySource
    void isBlank_ShouldReturnTrueForNullAndEmptyStrings(String input) {
        assertFalse(PasswordValidator.validate(input));
    }
    @ParameterizedTest
    @CsvSource({"hhAg1oih%,true", "tEst,false"})
    void toUpperCase_ShouldGenerateTheExpectedUppercaseValue(String input, Boolean expected) {
        Boolean actualValue = PasswordValidator.validate(input);
        assertEquals(expected, actualValue);
    }
}
