import org.apache.commons.lang3.StringUtils;

import java.util.Locale;


public class PasswordValidator {

    public static boolean containsLowerCase(String word){
        return !word.equals(word.toUpperCase());
    }

    public static boolean containsUpperCase(String word){
        return !word.equals(word.toLowerCase());
    }

    public static boolean validate(String word) {
        if (StringUtils.length(word) >= 8 && StringUtils.length(word) <= 20
                && StringUtils.containsAny(word, "@#$%")
                && PasswordValidator.containsLowerCase(word)
                && PasswordValidator.containsUpperCase(word)) {
            return true;
        }
        return false;
    }
}
