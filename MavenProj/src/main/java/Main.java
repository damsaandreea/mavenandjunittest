import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        String word = null;
        System.out.println("Introduce a word:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        word = reader.readLine();
        System.out.println(PasswordValidator.validate(word));
    }
}
